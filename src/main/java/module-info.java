module bsu.edu.ru.airport {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires javafx.base;
    requires javafx.graphics;

    opens bsu.edu.ru.airport to javafx.fxml;
    exports bsu.edu.ru.airport;
    exports bsu.edu.ru.airport.utils;
    opens bsu.edu.ru.airport.utils to javafx.fxml;
    exports bsu.edu.ru.airport.resource;
    opens bsu.edu.ru.airport.resource to javafx.fxml;
}