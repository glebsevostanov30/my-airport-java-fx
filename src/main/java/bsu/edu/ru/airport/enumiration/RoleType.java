package bsu.edu.ru.airport.enumiration;

public enum RoleType {
    EDIT, ADD, VIEW, DELETE
}
