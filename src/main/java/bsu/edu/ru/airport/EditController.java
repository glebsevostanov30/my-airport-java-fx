package bsu.edu.ru.airport;

import bsu.edu.ru.airport.resource.FlightTableResource;
import bsu.edu.ru.airport.resource.EditResource;
import bsu.edu.ru.airport.utils.ButtonUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class EditController implements Initializable {

    @FXML
    private VBox vBox;
    @FXML
    private TextField numberFlight;
    @FXML
    private TextField from;
    @FXML
    private TextField to;
    @FXML
    private TextField dateFrom;
    @FXML
    private TextField dateTill;
    @FXML
    private TextField numberStripes;
    @FXML
    private Button closeButton;
    @FXML
    private Button deleteButton;


    private EditResource editResource;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        editResource = (EditResource) resourceBundle;

        if (editResource == null) {
            return;
        }

        if (editResource.getId() == null) {
            deleteButton.setVisible(false);
        }

        if (editResource.getFlightTableResourceTableView() == null) {
            return;
        }

        var flightTableResources = editResource.flightTableResourceTableView.getItems();

        if (editResource.getId() != null) {
            var optionalFlightTableResource = flightTableResources.stream().filter(dto -> editResource.getId().equals(dto.getId())).findFirst();
            if(optionalFlightTableResource.isPresent()){
                var flightTableResource = optionalFlightTableResource.get();
                numberFlight.setText(flightTableResource.getNumberFlight());
                from.setText(flightTableResource.getFrom());
                to.setText(flightTableResource.getTo());
                dateFrom.setText(flightTableResource.getDateFrom());
                dateTill.setText(flightTableResource.getDateTill());
                numberStripes.setText(String.valueOf(flightTableResource.getNumberStripes()));
            }
        }
    }

    @FXML
    protected void buttonSave() throws IOException {
        if (editResource.getFlightTableResourceTableView() == null) {
            return;
        }

        var flightTableResourceList = editResource.getFlightTableResourceTableView().getItems();
        flightTableResourceList.removeIf(flightTableResource -> flightTableResource.getId() == null);


        if (editResource.getId() != null) {
            var optionalFlightTableResource = flightTableResourceList.stream()
                    .filter(dto -> editResource.getId().equals(dto.getId()))
                    .findFirst();
            if(optionalFlightTableResource.isPresent()){
                var flightTableResource = optionalFlightTableResource.get();
                flightTableResource.setNumberFlight(numberFlight.getText());
                flightTableResource.setFrom(from.getText());
                flightTableResource.setTo(to.getText());
                flightTableResource.setDateFrom(dateFrom.getText());
                flightTableResource.setDateTill(dateTill.getText());
                flightTableResource.setNumberStripes(Integer.parseInt(numberStripes.getText()));
            }
        }

        if (editResource.getId() == null) {
            var index = (long) flightTableResourceList.size();
            LocalDateTime localDateTimeFrom = LocalDateTime.parse(dateFrom.getText(), FlightTableResource.dateTimeFormatter);
            LocalDateTime localDateTimeTill = LocalDateTime.parse(dateTill.getText(), FlightTableResource.dateTimeFormatter);
            var flightTableResource = new FlightTableResource(index + 1, numberFlight.getText(), from.getText(), to.getText(), localDateTimeFrom, localDateTimeTill, Integer.parseInt(numberStripes.getText()));

            ButtonUtils.addEventNextFrame(flightTableResource.getButtonEdit(), EditController.class.getResource("edit.fxml"), editResource.getFlightTableResourceTableView(), index + 1);
            ButtonUtils.addEventNextFrame(flightTableResource.getButtonAdd(), EditController.class.getResource("edit.fxml"), editResource.getFlightTableResourceTableView(), null);

            flightTableResourceList.add(flightTableResource);
        }


        buttonClose();
    }

    @FXML
    protected void buttonClose() throws IOException {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    protected void buttonDelete() throws IOException {
        if (editResource == null) {
            return;
        }

        if (editResource.getFlightTableResourceTableView() == null) {
            return;
        }

        var flightTableResourceList = editResource.getFlightTableResourceTableView().getItems();

        flightTableResourceList.removeIf(flightTableResource -> flightTableResource.getId() == editResource.getId());

        if(flightTableResourceList.size() == 0){
            var flightTableResource = new FlightTableResource();
            ButtonUtils.addEventNextFrame(flightTableResource.getButtonAdd(), EditController.class.getResource("edit.fxml"), editResource.getFlightTableResourceTableView(), null);
            flightTableResourceList.add(flightTableResource);
        }

        buttonClose();
    }
}
