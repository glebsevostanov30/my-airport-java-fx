package bsu.edu.ru.airport.utils;

import bsu.edu.ru.airport.resource.FlightTableResource;
import bsu.edu.ru.airport.resource.EditResource;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class ButtonUtils {
    public static void addEventNextFrame(Button button, URL resource, TableView<FlightTableResource> flightTableResourceTableView, Long id){
        EditResource editResource;
        if(id != null){
            editResource = new EditResource(flightTableResourceTableView, id);
        }else {
            editResource = new EditResource(flightTableResourceTableView);
        }
        EditResource finalEditResource = editResource;
        button.setOnAction(event -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(resource, finalEditResource);
                Scene scene = new Scene(fxmlLoader.load(), 720, 720);
                var stage = new Stage();
                stage.setTitle("Редактирование");
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
