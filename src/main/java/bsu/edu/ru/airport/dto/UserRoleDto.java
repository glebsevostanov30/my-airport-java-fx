package bsu.edu.ru.airport.dto;

import bsu.edu.ru.airport.enumiration.RoleType;

import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;

public class UserRoleDto extends ResourceBundle {
    private final List<RoleType> roleTypes;

    public UserRoleDto(List<RoleType> roleTypes) {
        this.roleTypes = roleTypes;
    }

    @Override
    protected Object handleGetObject(String s) {
        return roleTypes;
    }

    @Override
    public Enumeration<String> getKeys() {
        return null;
    }

    public List<RoleType> getRoleTypes() {
        return roleTypes;
    }
}
