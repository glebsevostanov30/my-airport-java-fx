package bsu.edu.ru.airport.dto;


import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class UserDto {
    private TextField textFieldLogin;
    private PasswordField textFieldPassword;

    public UserDto(TextField textFieldLogin, PasswordField textFieldPassword) {
        this.textFieldLogin = textFieldLogin;
        this.textFieldPassword = textFieldPassword;
    }
    public TextField getTextFieldLogin() {
        return textFieldLogin;
    }

    public void setTextFieldLogin(TextField textFieldLogin) {
        this.textFieldLogin = textFieldLogin;
    }

    public PasswordField getTextFieldPassword() {
        return textFieldPassword;
    }

    public void setTextFieldPassword(PasswordField textFieldPassword) {
        this.textFieldPassword = textFieldPassword;
    }
}
