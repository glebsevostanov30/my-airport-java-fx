package bsu.edu.ru.airport;

import bsu.edu.ru.airport.dto.UserDto;
import bsu.edu.ru.airport.service.AuthUserService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class AuthController {
    @FXML
    public VBox vbox;

    @FXML
    private TextField textFieldLogin;

    @FXML
    private PasswordField textFieldPassword;

    @FXML
    protected void onButtonAuth() throws IOException {
        var user = new UserDto(textFieldLogin, textFieldPassword);

        AuthUserService userService = new AuthUserService();
        var userRole = userService.auth(user);

        Parent newRoot = FXMLLoader.load(AuthController.class.getResource("table-view.fxml"), userRole);
        vbox.getScene().setRoot(newRoot);
    }
}
