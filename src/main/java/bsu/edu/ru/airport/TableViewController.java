package bsu.edu.ru.airport;

import bsu.edu.ru.airport.dto.UserRoleDto;
import bsu.edu.ru.airport.enumiration.RoleType;
import bsu.edu.ru.airport.resource.FlightTableResource;
import bsu.edu.ru.airport.utils.ButtonUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class TableViewController implements Initializable {

    @FXML
    private VBox vBox;
    @FXML
    private TableView<FlightTableResource> flightTable;
    @FXML
    private TableColumn<FlightTableResource, String> numberFlight;
    @FXML
    private TableColumn<FlightTableResource, String> from;
    @FXML
    private TableColumn<FlightTableResource, String> to;
    @FXML
    private TableColumn<FlightTableResource, String> dateFrom;
    @FXML
    private TableColumn<FlightTableResource, String> dateTill;
    @FXML
    private TableColumn<FlightTableResource, String> numberStripes;
    @FXML
    private TableColumn<FlightTableResource, Button> buttonEdit;
    @FXML
    private TableColumn<FlightTableResource, Button> buttonAdd;


    @FXML
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        numberFlight.setCellValueFactory(
                new PropertyValueFactory<>("numberFlight"));
        from.setCellValueFactory(
                new PropertyValueFactory<>("from"));
        to.setCellValueFactory(
                new PropertyValueFactory<>("to"));
        dateFrom.setCellValueFactory(
                new PropertyValueFactory<>("dateFrom"));
        dateTill.setCellValueFactory(
                new PropertyValueFactory<>("dateTill"));
        numberStripes.setCellValueFactory(
                new PropertyValueFactory<>("numberStripes"));
        buttonEdit.setCellValueFactory(
                new PropertyValueFactory<>("buttonEdit"));
        buttonAdd.setCellValueFactory(
                new PropertyValueFactory<>("buttonAdd"));

        var userRoleDto = ((UserRoleDto) resourceBundle);

        var isAdded = userRoleDto.getRoleTypes().stream().anyMatch(RoleType.ADD::equals);
        var isEdit = userRoleDto.getRoleTypes().stream().anyMatch(RoleType.EDIT::equals);
        var isView = userRoleDto.getRoleTypes().stream().anyMatch(RoleType.VIEW::equals);

        flightTable.setVisible(isView);
        buttonEdit.setVisible(isEdit);
        buttonAdd.setVisible(isAdded);

        final ObservableList<FlightTableResource> data = FXCollections.observableArrayList(
                new FlightTableResource(1L, "123", "Прохоровка", "Белгород", LocalDateTime.now(), LocalDateTime.now(), 1),
                new FlightTableResource(2L, "234", "Анапа", "Старый оскол", LocalDateTime.now(), LocalDateTime.now(), 2),
                new FlightTableResource(3L, "345", "Белгород", "Москва", LocalDateTime.now(), LocalDateTime.now(), 3),
                new FlightTableResource(4L, "567", "Москва", "Краснодар", LocalDateTime.now(), LocalDateTime.now(), 1)
        );

        flightTable.setItems(data);

        data.forEach(dto -> ButtonUtils.addEventNextFrame(dto.getButtonEdit(), EditController.class.getResource("edit.fxml"), flightTable, dto.getId()));
        data.forEach(dto -> ButtonUtils.addEventNextFrame(dto.getButtonAdd(), EditController.class.getResource("edit.fxml"), flightTable, null));
    }
}
