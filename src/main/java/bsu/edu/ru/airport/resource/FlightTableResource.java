package bsu.edu.ru.airport.resource;


import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;
import java.util.ResourceBundle;

public class FlightTableResource extends ResourceBundle {
    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    private final Long id;
    private final SimpleStringProperty numberFlight;
    private final SimpleStringProperty from;
    private final SimpleStringProperty to;
    private final SimpleStringProperty dateFrom;
    private final SimpleStringProperty dateTill;
    private final SimpleIntegerProperty numberStripes;
    private final SimpleObjectProperty<Button> buttonEdit;
    private final SimpleObjectProperty<Button> buttonAdd;

    public FlightTableResource(long id, String numberFlight, String from, String to, LocalDateTime dateFrom, LocalDateTime dateTill, Integer numberStripes) {
        this.id = id;
        this.numberFlight = new SimpleStringProperty(numberFlight);
        this.from = new SimpleStringProperty(from);
        this.to = new SimpleStringProperty(to);
        this.dateFrom = new SimpleStringProperty(dateTimeFormatter.format(dateFrom));
        this.dateTill = new SimpleStringProperty(dateTimeFormatter.format(dateTill));
        this.numberStripes = new SimpleIntegerProperty(numberStripes);

        var buttonAdd = new Button("Добавить");
        buttonAdd.setPrefSize(150,20);
        var buttonEdit = new Button("Редактировать");
        buttonEdit.setPrefSize(150,20);

        this.buttonAdd = new SimpleObjectProperty<>(buttonAdd);
        this.buttonEdit = new SimpleObjectProperty<>(buttonEdit);
    }

    public FlightTableResource(){
        this.id = null;
        this.numberFlight = new SimpleStringProperty();
        this.from = new SimpleStringProperty();
        this.to = new SimpleStringProperty();
        this.dateFrom = new SimpleStringProperty();
        this.dateTill = new SimpleStringProperty();
        this.numberStripes = new SimpleIntegerProperty();
        this.buttonEdit = null;
        var buttonAdd = new Button("Добавить");
        buttonAdd.setPrefSize(150,20);
        this.buttonAdd = new SimpleObjectProperty<>(buttonAdd);
    }


    public Button getButtonAdd() {
        return buttonAdd.get();
    }

    public SimpleObjectProperty<Button> buttonAddProperty() {
        return buttonAdd;
    }

    public void setButtonAdd(Button buttonAdd) {
        this.buttonAdd.set(buttonAdd);
    }
    public Button getButtonEdit() {
        return buttonEdit.get();
    }

    public SimpleObjectProperty<Button> buttonEditProperty() {
        return buttonEdit;
    }

    public void setButtonEdit(Button buttonEdit) {
        this.buttonEdit.set(buttonEdit);
    }

    public String getNumberFlight() {
        return numberFlight.get();
    }

    public SimpleStringProperty numberFlightProperty() {
        return numberFlight;
    }

    public void setNumberFlight(String numberFlight) {
        this.numberFlight.set(numberFlight);
    }

    public String getFrom() {
        return from.get();
    }

    public SimpleStringProperty fromProperty() {
        return from;
    }

    public void setFrom(String from) {
        this.from.set(from);
    }

    public String getTo() {
        return to.get();
    }

    public SimpleStringProperty toProperty() {
        return to;
    }

    public void setTo(String to) {
        this.to.set(to);
    }

    public String getDateFrom() {
        return dateFrom.get();
    }

    public SimpleStringProperty dateFromProperty() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom.set(dateFrom);
    }

    public String getDateTill() {
        return dateTill.get();
    }

    public SimpleStringProperty dateTillProperty() {
        return dateTill;
    }

    public void setDateTill(String dateTill) {
        this.dateTill.set(dateTill);
    }

    public int getNumberStripes() {
        return numberStripes.get();
    }

    public SimpleIntegerProperty numberStripesProperty() {
        return numberStripes;
    }

    public void setNumberStripes(int numberStripes) {
        this.numberStripes.set(numberStripes);
    }

    public Long getId() {
        return id;
    }


    @Override
    protected Object handleGetObject(String s) {
        return this;
    }

    @Override
    public Enumeration<String> getKeys() {
        return null;
    }
}
