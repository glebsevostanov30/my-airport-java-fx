package bsu.edu.ru.airport.resource;

import javafx.scene.control.TableView;

import java.util.ListResourceBundle;

public class EditResource extends ListResourceBundle {
    public final TableView<FlightTableResource> flightTableResourceTableView;
    public Long id;


    public TableView<FlightTableResource> getFlightTableResourceTableView() {
        return flightTableResourceTableView;
    }


    public EditResource(TableView<FlightTableResource> flightTableResourceTableView, Long id) {
        this.flightTableResourceTableView = flightTableResourceTableView;
        this.id = id;
    }

    public EditResource(TableView<FlightTableResource> flightTableResourceTableView) {
        this.flightTableResourceTableView = flightTableResourceTableView;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Override
    protected Object[][] getContents() {
        return new Object[][] {new TableView[]{flightTableResourceTableView}};
    }
}
