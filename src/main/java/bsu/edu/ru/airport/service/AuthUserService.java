package bsu.edu.ru.airport.service;

import bsu.edu.ru.airport.dto.UserDto;
import bsu.edu.ru.airport.dto.UserRoleDto;
import bsu.edu.ru.airport.enumiration.RoleType;

import java.util.ArrayList;

public class AuthUserService {
    public UserRoleDto auth(UserDto userDto){
        var userRoles = new ArrayList<RoleType>();

        var login = userDto.getTextFieldLogin().getText();
        var password = userDto.getTextFieldPassword().getText();

        if(login.equals("123") && password.equals("123")){
            userRoles.add(RoleType.ADD);
            userRoles.add(RoleType.EDIT);
            userRoles.add(RoleType.VIEW);
        }

        if(login.equals("555") && password.equals("555")){
            userRoles.add(RoleType.VIEW);
        }

        return new UserRoleDto(userRoles);
    }
}
